import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ImageInput extends StatefulWidget {
  const ImageInput({super.key, required this.onselect});
  final Function(File file) onselect;

  @override
  State<ImageInput> createState() => _ImagePickerState();
}

class _ImagePickerState extends State<ImageInput> {
  // ignore: unused_field
  File? _selectedImage;
  Future<void> takePick() async {
    final ImagePicker picker = ImagePicker();
    final takeImage =
        await picker.pickImage(source: ImageSource.camera, maxWidth: 600);
    if (takeImage == null) {
      return;
    }

    setState(() {
      _selectedImage = File(takeImage.path);
    });
    widget.onselect(_selectedImage!);
  }

  @override
  Widget build(BuildContext context) {
    Widget content = TextButton.icon(
      onPressed: takePick,
      icon: const Icon(Icons.camera),
      label: Text('Take Picture',
          style: Theme.of(context)
              .textTheme
              .bodyLarge!
              .copyWith(color: Theme.of(context).colorScheme.primaryContainer)),
    );

    if (_selectedImage != null) {
      content = GestureDetector(
        onTap: takePick,
        child: Image.file(
          _selectedImage!,
          fit: BoxFit.cover,
          height: double.infinity,
          width: double.infinity,
        ),
      );
    }
    return Container(
      decoration: BoxDecoration(
        border: Border.all(
          width: 1,
          color: Theme.of(context).colorScheme.primary.withOpacity(0.2),
        ),
      ),
      height: 250,
      width: double.infinity,
      alignment: Alignment.center,
      child: content,
    );
  }
}
