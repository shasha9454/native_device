import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:native_device/place.dart';

class MapScreen extends StatefulWidget {
  const MapScreen(
      {super.key,
      this.location = const PlaceLocation(
          latitude: 37.422, longitude: -122.084, address: ''),
      this.isselecting = true});

  final PlaceLocation location;
  final bool isselecting;

  @override
  State<MapScreen> createState() {
    return _MapScreenState();
  }
}

class _MapScreenState extends State<MapScreen> {
  @override
  Widget build(BuildContext context) {
    LatLng? pickedLocation;
    return Scaffold(
      appBar: AppBar(
        title:
            Text(widget.isselecting ? 'Pick your Location' : 'Your Location'),
        actions: [
          if (widget.isselecting)
            IconButton(
                onPressed: () {
                  Navigator.of(context).pop(pickedLocation);
                },
                icon: const Icon(Icons.save))
        ],
      ),
      body: GoogleMap(
          onTap: !widget.isselecting
              ? null
              : (position) {
                  pickedLocation = position;
                },
          initialCameraPosition: CameraPosition(
            target: LatLng(widget.location.latitude, widget.location.longitude),
            zoom: 16,
          ),
          // ignore: unnecessary_null_comparison
          markers: (pickedLocation == null && widget.isselecting)
              ? {}
              : {
                  Marker(
                      markerId: const MarkerId('1'),
                      position: pickedLocation ??
                          LatLng(widget.location.latitude,
                              widget.location.longitude)),
                }),
    );
  }
}
