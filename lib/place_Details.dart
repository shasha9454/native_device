// ignore: file_names

import 'package:flutter/material.dart';
import 'package:native_device/map.dart';
import 'package:native_device/place.dart';

// ignore: camel_case_types
class placeDetails extends StatelessWidget {
  const placeDetails({super.key, required this.list});
  final Place list;
  String get locationimage {
    final lat = list.location.latitude;
    final lon = list.location.longitude;
    return 'https://maps.googleapis.com/maps/api/staticmap?center=$lat,$lon=Brooklyn+Bridge,New+York,NY&zoom=16&size=600x300&maptype=roadmap&markers=color:red%7Clabel:A%7C$lat,$lon&key=AIzaSyCf0d0oAEHfso-zQ8DhIpv_axNyDjP98VU';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(list.title)),
        body: Stack(
          children: [
            Image.file(
              list.image,
              fit: BoxFit.cover,
              height: double.infinity,
              width: double.infinity,
            ),
            Positioned(
                right: 0,
                bottom: -3,
                left: 1,
                child: Column(
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (btx) => MapScreen(
                                  location: list.location,
                                  isselecting: false,
                                )));
                      },
                      child: CircleAvatar(
                        radius: 60,
                        backgroundImage: NetworkImage(locationimage),
                      ),
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Container(
                      decoration: const BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            Colors.transparent,
                            Colors.black54,
                          ],
                          begin: Alignment.bottomCenter,
                          end: Alignment.topCenter,
                        ),
                      ),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 26, vertical: 29),
                      alignment: Alignment.center,
                      child: Text(
                        list.location.address,
                        style: Theme.of(context).textTheme.titleLarge!.copyWith(
                            color: Theme.of(context).colorScheme.onBackground),
                      ),
                    )
                  ],
                ))
          ],
        ));
  }
}
