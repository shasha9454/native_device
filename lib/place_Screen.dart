import 'package:flutter/material.dart';
import 'package:native_device/title_provider.dart';

import 'package:native_device/place_getter.dart';
import 'package:native_device/user_input.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class PlaceScreen extends ConsumerStatefulWidget {
  const PlaceScreen({super.key});

  @override
  ConsumerState<ConsumerStatefulWidget> createState() {
    return _PlaceScreenState();
  }
}

class _PlaceScreenState extends ConsumerState<PlaceScreen> {
  late Future<void> _getFutureRespnse;
  @override
  void initState() {
    _getFutureRespnse = ref.read(providerPlace.notifier).loadPlaces();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final list = ref.watch(providerPlace);
    Widget content = Center(
      child: Text(
        "No Content Avaiable!",
        style: Theme.of(context)
            .textTheme
            .bodyLarge!
            .copyWith(color: Theme.of(context).colorScheme.primaryContainer),
      ),
    );

    if (list.isNotEmpty) {
      content = PlaceGet(place: list);
    }
    return Scaffold(
      appBar: AppBar(
        title: const Text('Place-Screen'),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.of(context)
                    // ignore: prefer_const_constructors
                    .push(MaterialPageRoute(builder: (ctx) => userInfo()));
              },
              icon: const Icon(Icons.add))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: FutureBuilder(
            future: _getFutureRespnse,
            builder: ((context, snapshot) =>
                snapshot.connectionState == ConnectionState.waiting
                    ? const CircularProgressIndicator()
                    : content)),
      ),
    );
  }
}
