import 'package:flutter/material.dart';
import 'package:native_device/place.dart';
import 'package:native_device/place_Details.dart';

class PlaceGet extends StatelessWidget {
  const PlaceGet({super.key, required this.place});
  final List<Place> place;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        padding: const EdgeInsets.only(bottom: 10, top: 10),
        itemCount: place.length,
        itemBuilder: (context, index) => ListTile(
              contentPadding: const EdgeInsets.all(8),
              leading: CircleAvatar(
                radius: 26,
                backgroundImage: FileImage(place[index].image),
              ),
              title: Text(place[index].title,
                  style: Theme.of(context).textTheme.bodyLarge!.copyWith(
                      color: Theme.of(context).colorScheme.primaryContainer)),
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (ctx) => placeDetails(
                          list: place[index],
                        )));
              },
            ));
  }
}
