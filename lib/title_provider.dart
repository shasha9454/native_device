import 'dart:io';
import 'package:path_provider/path_provider.dart' as syspath;
import 'package:native_device/place.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart' as sql;
import 'package:sqflite/sqlite_api.dart';

// ignore: unused_element
Future<Database> _getDatabase() async {
  final dbpath = await sql.getDatabasesPath();
  final db = await sql.openDatabase(
    path.join(dbpath, 'Places_user'),
    onCreate: (db, version) => db.execute(
        'CREATE TABLE user_places(id TEXT PRIMARY KEY ,title TEXT , image TEXT , lat REAL , lng REAL , address TEXT )'),
    version: 1,
  );
  return db;
}

// ignore: camel_case_types
class title_Provider extends StateNotifier<List<Place>> {
  title_Provider() : super(const []);

  Future<void> loadPlaces() async {
    final db = await _getDatabase();
    final data = await db.query('user_places');
    final places = data
        .map(
          (item) => Place(
            id: item['id'] as String,
            title: item['title'] as String,
            image: File(item['image'] as String),
            location: PlaceLocation(
                latitude: item['lat'] as double,
                longitude: item['lng'] as double,
                address: item['address'] as String),
          ),
        )
        .toList();
    state = places;
  }

  Future<void> addPlace(
      String title, File image, PlaceLocation location) async {
    final appDir = await syspath.getApplicationDocumentsDirectory();
    final filename = path.basename(image.path);
    final copiedImage = await image.copy('${appDir.path}/$filename');
    var newPlace = Place(title: title, image: copiedImage, location: location);
    state = [newPlace, ...state];

    final db = await _getDatabase();
    db.insert('user_places', {
      'id': newPlace.id,
      'title': newPlace.title,
      'image': newPlace.image.path,
      'lat': newPlace.location.latitude,
      'lng': newPlace.location.longitude,
      'address': newPlace.location.address
    });
  }
}

final providerPlace = StateNotifierProvider<title_Provider, List<Place>>(
    (ref) => title_Provider());
