import 'dart:io';
import 'package:flutter/material.dart';
import 'package:native_device/image_picker.dart';
import 'package:native_device/location_input.dart';
import 'package:native_device/place.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:native_device/title_provider.dart';

// ignore: camel_case_types
class userInfo extends ConsumerStatefulWidget {
  const userInfo({super.key});
  @override
  ConsumerState<userInfo> createState() {
    return _userInfoState();
  }
}

// ignore: camel_case_types
class _userInfoState extends ConsumerState<userInfo> {
  File? _selectedimage;
  // ignore: unused_field
  PlaceLocation? _selectedLocation;
  final _txtController = TextEditingController();

  void saveinfo() {
    final input = _txtController.text;
    if (input.isEmpty || _selectedimage == null || _selectedLocation == null) {
      return;
    }
    // ref
    //     .read(placeprovider.notifier)
    //     .addPLace(input, _selectedimage!, _selectedLocation!);
    // Navigator.of(context).pop();
    ref
        .read(providerPlace.notifier)
        .addPlace(input, _selectedimage!, _selectedLocation!);
  }

  @override
  void dispose() {
    super.dispose();
    _txtController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('User Input'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(12),
        child: Column(
          children: [
            const SizedBox(
              width: double.infinity,
            ),
            TextField(
                controller: _txtController,
                maxLength: 20,
                decoration: InputDecoration(
                    fillColor: Colors.white,
                    filled: true,
                    hintText: 'enter Your Title here!',
                    border: InputBorder.none,
                    label: Text(
                      'Title',
                      style: Theme.of(context).textTheme.titleMedium!.copyWith(
                            color: Theme.of(context).colorScheme.primary,
                            fontSize: 20,
                          ),
                    )),
                style: Theme.of(context).textTheme.bodyMedium),
            ImageInput(
              onselect: (image) {
                _selectedimage = image;
              },
            ),
            const SizedBox(
              height: 10,
            ),
            locationInput(
              onSelectLocation: (location) {
                _selectedLocation = location;
              },
            ),
            const SizedBox(
              height: 10,
            ),
            const SizedBox(
              height: 17,
            ),
            ElevatedButton(onPressed: saveinfo, child: const Text("submit"))
          ],
        ),
      ),
    );
  }
}
